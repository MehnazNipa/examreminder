angular.module('app.routes', [])

  .config(function ($stateProvider, $urlRouterProvider) {

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider

    /* .state('registration', {
     url: '/registration',
     templateUrl: 'templates/registration.html',
     controller: 'registrationCtrl'
     })*/


      .state('tabsController', {
        url: '/tab',
        templateUrl: 'templates/tabsController.html',
        abstract: true
      })

      .state('tabsController.governmentJobExam', {
        url: '/governmentJobExam',
        views: {
          'tab1': {
            templateUrl: 'templates/governmentJobExam.html',
            controller: 'examReminderCtrl'
          }
        }
      })

      .state('tabsController.nonGovernmentJobExam', {
        url: '/nonGovernmentJobExam',
        views: {
          'tab2': {
            templateUrl: 'templates/nonGovernmentJobExam.html',
            controller: 'examReminderCtrl'
          }
        }
      })

      .state('tabsController.academicExam', {
        url: '/academicExam',
        views: {
          'tab3': {
            templateUrl: 'templates/academicExam.html',
            controller: 'examReminderCtrl'
          }
        }
      });


    $urlRouterProvider.otherwise('/tab/governmentJobExam')


  });

angular.module('app.controllers', [])

  .controller('examReminderCtrl', ['$rootScope', '$scope', '$stateParams', '$ionicModal',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller

    function ($rootScope, $scope, $stateParams, $ionicModal) {

      $scope.content = 'job';
      $scope.status = 'applied';
      $scope.job = {};
      $scope.jobList = JSON.parse(localStorage.getItem('job')) || [];
      $scope.academic = {};
      $scope.academicList = JSON.parse(localStorage.getItem('academic')) || [];

      // Load the addItem modal from the given template URL
      $ionicModal.fromTemplateUrl('templates/addItem.html', {

        scope: $scope,
        animation: 'slide-in-up'

      }).then(function (modal) {
        $scope.addItem = modal;
      });


      /**
       * this function add job item
       * @param jobExmData
       */
      $scope.addJobExamItem = function (jobExmData) {

        $scope.jobList.push(jobExmData);

        localStorage.setItem('job', JSON.stringify($scope.jobList));
     
        $scope.job = {};
        $scope.addItem.hide();
        $rootScope.$broadcast('examData');

      };

      /**
       * this function add academic item
       * @param academicExmData
       */
      $scope.addAcademicItem = function (academicExmData) {

        $scope.academicList.push(academicExmData);
        console.log($scope.academicList);

        localStorage.setItem('academic', JSON.stringify($scope.academicList));

        $scope.academic = {};
        $scope.addItem.hide();
        $rootScope.$broadcast('examData');

      };


      $rootScope.$on('examData', function () {
        $scope.jobList = JSON.parse(localStorage.getItem('job'));
        $scope.academicList = JSON.parse(localStorage.getItem('academic'));
      });


    }]);
